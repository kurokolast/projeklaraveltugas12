<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('halaman.biodata');
    }

    
    public function kirim(Request $request){
        // dd($request->all());
        $nama = $request->nama;
        $biodata = $request->bio;

        return view('halaman.welcome', compact('nama','biodata'));
    }
}
