<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>REGISTER</title>
</head>
<body>
<h1>Buat Account Baru!</h1>

<h3>Sign Up Form</h3>

<form action="/go" method="post">
    @csrf
    <label>First Name :</label> <br>
    <input type="text" name="nama"> <br> <br>
    <label>Last Name :</label> <br>
    <input type="text" name="nama2"> <br> <br>
    <label>Gender</label> <br> <br>
    <input type="radio" name="g" value="Male"> Male <br>
    <input type="radio" name="g" value="Female"> Female <br>
    <input type="radio" name="g" value="Other"> Other <br> <br>
    <label>Nationality:</label> <br> <br>
    <select name="G">
        <option value="1">Indonesia</option>
        <option value="2">Amerika</option>
        <option value="3">Inggris</option>
    </select> <br> <br>
    <label>Language Spoken</label> <br> <br>
    <input type="checkbox" name="ls"> Bahasa Indonesia <br>
    <input type="checkbox" name="ls"> English <br>
    <input type="checkbox" name="ls"> Other <br> <br>
    <label>Bio</label> <br> <br>
    <textarea name="bio" cols="30" rows="10"></textarea> <br>
    
    <input type="submit" value="Sign up">
</form>
</body>
</html>