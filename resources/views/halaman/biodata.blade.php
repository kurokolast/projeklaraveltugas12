@extends('layout.master')

@section('judul')
Halaman Biodata
@endsection

@section('content')
    <form action="/go" method="post">
    @csrf
    <label>First Name :</label> <br>
    <input type="text" name="nama"> <br> <br>
    <label>Last Name :</label> <br>
    <input type="text" name="nama2"> <br> <br>
    <label>Gender</label> <br> <br>
    <input type="radio" name="g" value="Male"> Male <br>
    <input type="radio" name="g" value="Female"> Female <br>
    <input type="radio" name="g" value="Other"> Other <br> <br>
    <label>Nationality:</label> <br> <br>
    <select name="G">
        <option value="1">Indonesia</option>
        <option value="2">Amerika</option>
        <option value="3">Inggris</option>
    </select> <br> <br>
    <label>Language Spoken</label> <br> <br>
    <input type="checkbox" name="ls"> Bahasa Indonesia <br>
    <input type="checkbox" name="ls"> English <br>
    <input type="checkbox" name="ls"> Other <br> <br>
    <label>Bio</label> <br> <br>
    <textarea name="bio" cols="30" rows="10"></textarea> <br>
    
    <input type="submit" value="Sign up">
    </form>
@endsection