<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/form', 'AuthController@form');

Route::get('/data-tables', function(){
    return view('table.datatable');
});

Route::get('/table', function(){
    return view('table.table');
});

Route::post('/go', 'AuthController@kirim');